//
//  MigracionPesada.swift
//  MisNotas
//
//  Created by Master Móviles on 23/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

class MigracionPesada: NSEntityMigrationPolicy {
    override func createDestinationInstances(forSource sInstance: NSManagedObject, in mapping: NSEntityMapping, manager: NSMigrationManager) throws {
        //sacamos el contenido de la nota actual
        let notaOrigen = sInstance
        let cad = notaOrigen.value(forKey: "contenido") as! String
        let fecha = notaOrigen.value(forKey: "fecha")
        let tags = notaOrigen.value(forKey: "tags")
        
        //sacar una subcadena es rarito porque no se trabaja con pos. numéricas
        //sino con la clase Index
        var offset = 10
        
        if cad.characters.count < 10 {
            offset = cad.characters.count
        }
        
        let index = cad.index(cad.startIndex, offsetBy: offset)
        let tit = cad.substring(to: index)
        
        //creamos una nota de la nueva versión, no podemos usar la
        //clase Nota, esa es la antigua
        let notaDestino = NSEntityDescription.insertNewObject(forEntityName: "Nota", into: manager.destinationContext)
        // - usando setValue, fijar la propiedad "titulo" de "notaDestino" a "tit"
        notaDestino.setValue(tit, forKey: "titulo")
        notaDestino.setValue(cad, forKey: "contenido")
        notaDestino.setValue(tags, forKey: "tags")
        notaDestino.setValue(fecha, forKey: "fecha")
        
        //decimos que la nueva versión de "notaOrigen" es "notaDestino"
        manager.associate(sourceInstance: notaOrigen, withDestinationInstance: notaDestino, for: mapping)
    }
    
    
}
