//
//  Nota+CoreDataProperties.swift
//  MisNotas
//
//  Created by Master Móviles on 23/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation
import CoreData


extension Nota {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Nota> {
        return NSFetchRequest<Nota>(entityName: "Nota");
    }

    @NSManaged public var contenido: String?
    @NSManaged public var fecha: NSDate?
    @NSManaged public var tags: [String]?
    @NSManaged public var titulo: String?

}
