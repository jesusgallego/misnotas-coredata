//
//  Nota+Custom.swift
//  MisNotas
//
//  Created by Master Móviles on 19/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import Foundation

extension Nota {
    var inicial: String? {
        if let textToNil = self.contenido {
            let pos = textToNil.index(after: textToNil.startIndex)
            return textToNil.substring(to: pos)
        } else {
            return nil
        }
    }
}
