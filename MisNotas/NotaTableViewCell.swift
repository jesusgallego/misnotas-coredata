//
//  NotaTableViewCell.swift
//  MisNotas
//
//  Created by Master Móviles on 18/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit

class NotaTableViewCell: UITableViewCell {

    @IBOutlet weak var textoLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
