//
//  ViewController.swift
//  MisNotas
//
//  Created by Master Móviles on 11/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var tagsTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        clear()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func clear() {
        //Date label
        dateLabel.text = ""
        
        // Text View
        textView.text = ""
        
        // Tags
        tagsTextField.text = ""
        
        // Notification
        notificationLabel.backgroundColor = UIColor.white
        notificationLabel.text = ""
    }
    
    func errorNotification(withMessage message: String) {
        notificationLabel.backgroundColor = UIColor(red:1.00, green:0.73, blue:0.73, alpha:1.0)
        notificationLabel.textColor = UIColor(red:0.85, green:0.00, blue:0.05, alpha:1.0)
        notificationLabel.text = message
    }
    
    func successNotification(withMessage message: String) {
        notificationLabel.backgroundColor = UIColor(red:0.87, green:0.95, blue:0.75, alpha:1.0)
        notificationLabel.textColor = UIColor(red:0.31, green:0.54, blue:0.06, alpha:1.0)
        notificationLabel.text = message
    }

    @IBAction func onNuevaNotaClicked(_ sender: Any) {
        clear()
    }

    @IBAction func onGuardarNotaClicked(_ sender: Any) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let context = delegate.persistentContainer.viewContext
            
            let nuevaNota = NSEntityDescription.insertNewObject(forEntityName: "Nota", into: context) as! Nota
            
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"
            
            dateLabel.text = formatter.string(from: date)
        
            var tags: [String] = []
            if let tagsString = tagsTextField.text {
                tags = tagsString.components(separatedBy: " ")
            }
    
            nuevaNota.contenido = textView.text
            nuevaNota.fecha = date as NSDate
            nuevaNota.tags = tags
            
            do {
                try context.save()
                successNotification(withMessage: "La nota se ha guardado")
            } catch {
                errorNotification(withMessage: "Error al guardar: \(error)")
            }
            
        }
    }
}

