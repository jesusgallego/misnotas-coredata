//
//  TableViewController.swift
//  MisNotas
//
//  Created by Master Móviles on 11/1/17.
//  Copyright © 2017 Master Móviles. All rights reserved.
//

import UIKit
import CoreData

class TableViewController: UITableViewController, UISearchBarDelegate {
    
    var lista : [Nota]!
    
    var searchBar: UISearchBar?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        self.searchBar = UISearchBar()
        self.searchBar?.sizeToFit()
        self.searchBar?.delegate = self
        
        self.navigationItem.titleView = searchBar
        
        getNotasUniversity()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNotas()
    }
    
    func getNotas(filterBy text: String = "") {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let context = delegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<Nota>(entityName:"Nota")
            
            let dateSortDescriptor = NSSortDescriptor(key: "fecha", ascending: false)
            request.sortDescriptors = [dateSortDescriptor]
            
            if !text.isEmpty {
                request.predicate = NSPredicate(format: "texto CONTAINS[c] %@", argumentArray: [text])
            }
            
            if let notas = try? context.fetch(request) {
                self.lista = notas
                self.tableView.reloadData()
            }
        }
    }
    
    func getNotasUniversity() {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let context = delegate.persistentContainer.viewContext
            let model = delegate.persistentContainer.managedObjectModel
            
            let dictVars = ["tag": "#uni"]
            
            if let request = model.fetchRequestFromTemplate(withName: "Universidad", substitutionVariables: dictVars) {
                do {
                    let notas = try context.fetch(request) as! [Nota]
                    print("Notas con el tag #uni en el texto: ")
                    for nota in notas {
                        print(nota.contenido!)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return lista.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotaTableViewCell

        let nota = lista[indexPath.row]
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy hh:mm:ss"

        cell.textoLabel?.text = nota.contenido
        if let date = nota.fecha {
            cell.dateLabel.text = formatter.string(from: date as Date)
        }
        cell.tagsLabel.text = nota.tags?.joined(separator: ", ")

        return cell
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                let context = delegate.persistentContainer.viewContext
                
                // Delete from context
                let element = lista[indexPath.row]
                context.delete(element)
                
                do {
                    try context.save()
                    
                    let removed = lista.remove(at: indexPath.row)
                    
                    tableView.deleteRows(at: [indexPath], with: .fade)
                    
                    let actionSheet = UIAlertController(title: "",
                                                        message: "Se ha eliminado la fila",
                                                        preferredStyle: .actionSheet)
                    let ok = UIAlertAction(title: "Continuar", style: .default)
                    let deshacer = UIAlertAction(title: "Deshacer", style: .cancel) {
                        action in
                        // - llamar al "undo manager" para que deshaga la última operación
                        context.undo()
                        // - volver a insertar en el array la nota borrada, en la misma posición
                        self.lista.insert(removed, at: indexPath.row)
                        
                        //Esto ya está hecho, recargamos datos para que se repinten 
                        tableView.reloadData()
                    }
                    actionSheet.addAction(ok)
                    actionSheet.addAction(deshacer)
                    self.present(actionSheet, animated: true)
                    
                } catch {
                    
                }
            }
        }
    }
    
    // Mark: - SearchBarController
    /*
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("Search \(searchBar.text)")
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            let context = delegate.persistentContainer.viewContext
            
            let request = NSFetchRequest<Nota>(entityName: "Nota")
            
            if let queryText = searchBar.text, !queryText.isEmpty {
                request.predicate = NSPredicate(format: "texto CONTAINS[c] %@", argumentArray: [queryText])
            }
            
            if let notas = try? context.fetch(request) {
                self.lista = notas
                self.tableView.reloadData()
            }
        }
    }*/
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getNotas(filterBy: searchBar.text!)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
